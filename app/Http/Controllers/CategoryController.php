<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller {
  public function getCategories(Request $request)
  {
    return response(Category::all());
  }

  public function add(Request $request)
  {
    $title = $request->get('title');
    $parent_id = $request->get('parent_id');

    $category = new Category();
    $category->title = $title;
    $category->parent_id = $parent_id ? $parent_id : null;
    $category->save();

    return response('', 200);
  }

  public function update(Request $request)
  {
    $category = Category::find($request->get('id'));
    $category->title = $request->get('title');
    $category->parent_id = $request->get('parent_id');
    $category->save();

    return response('', 200);
  }

  public function delete(Request $request)
  {
    Category::destroy($request->get('id'));

    return response('', 200);
  }
}