<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FlickrController extends Controller
{
  public function getPhotos(Request $request)
  {
    $text = $request->get('text');
    $apiKey = env('FLICKR_API_KEY');

    $client = new Client();
    $response = $client->request(
      'GET',
      'https://api.flickr.com/services/rest',
      [
        'query' => [
          'method' => 'flickr.photos.search',
          'api_key' => $apiKey,
          'text' => $text,
          'format' => 'json',
          'nojsoncallback' => '1'
        ]
      ]
    );

    return response($response->getBody());
  }

  public function getPhotoDatas(Request $request)
  {
    $text = $request->get('text');
    $apiKey = env('FLICKR_API_KEY');

    $client = new Client();
    $response = $client->request(
      'GET',
      'https://api.flickr.com/services/rest',
      [
        'query' => [
          'method' => 'flickr.photos.getInfo',
          'api_key' => $apiKey,
          'photo_id' => $text,
          'format' => 'json',
          'nojsoncallback' => '1'
        ]
      ]
    );

    return response($response->getBody());
  }
}