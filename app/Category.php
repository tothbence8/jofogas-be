<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  public function Parent()
  {
    return $this->belongsTo(Category::class, 'parent_id', 'id');
  }
}
