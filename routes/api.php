<?php

Route::get('/photos', 'FlickrController@getPhotos');
Route::get('/photoDatas', 'FlickrController@getPhotoDatas');

Route::get('/categories', 'CategoryController@getCategories');
Route::post('/categories/add', 'CategoryController@add');
Route::post('/categories/update', 'CategoryController@update');
Route::post('/categories/delete', 'CategoryController@delete');
