FROM php:7.1-fpm

RUN docker-php-ext-install mysqli

RUN apt-get update && apt-get install -my wget gnupg libpng-dev libpq-dev

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y --force-yes unzip git ssl-cert nodejs libssl-dev

RUN curl -Ss https://getcomposer.org/installer | php && \
    mv composer.phar /usr/bin/composer

RUN apt-get install -y libmcrypt-dev \
    libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install mcrypt pdo pdo_mysql pdo_pgsql \
    && docker-php-ext-install gd
